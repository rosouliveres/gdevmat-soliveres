void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  populate();
}

BlackHole blackhole = new BlackHole();
Mover[] otherMatter = new Mover[200];

float counter = 0;
float timer = 300;

void draw()
{
  background(0);
  noStroke();
  
  counter++;
  
  if (counter >= timer)
  {
    blackhole.randomizeBlackHole();
    randomizeOtherMatter();
    
    counter = 0;
  }
  
  // Move OM to BH:
  for(int i = 0; i < otherMatter.length; i++)
  {
     PVector dir = PVector.sub(blackhole.position, otherMatter[i].position);
     
     // normalize the direction and multiply it by 10
     dir.normalize().mult(10);
       
     otherMatter[i].position.add(dir);
     
     otherMatter[i].render();
  }
    blackhole.render();
}

void populate()
{
   for(int i = 0; i < otherMatter.length; i++)
   {
     otherMatter[i] = new Mover(0, 0);
   }
}

void randomizeOtherMatter()
{
  float num = 50;
  for(int i = 0; i < otherMatter.length; i++)
    {
      float x_mean = random(-Window.windowWidth, Window.windowWidth);
      float y_mean = random(-Window.windowHeight, Window.windowHeight);
      
      otherMatter[i].position.x = randomGaussian() * num + x_mean;
      
      otherMatter[i].position.y = randomGaussian() * num + y_mean;
      
      otherMatter[i].setColor(random(255), random(255), random(255), 255);
      otherMatter[i].setScale(random(20, 40));
    }
}
