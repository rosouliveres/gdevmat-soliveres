public class BlackHole
{
  public PVector position;
  public float b_scale = 50;
  public float b_r = 255, b_g = 255, b_b = 255;
  public color blackHole_c = color(b_r, b_g, b_b);
  
  BlackHole()
  {
     position = new PVector();
  }
  
  void render()
  {
    fill(blackHole_c);
    circle(position.x, position.y, b_scale);    // Generates circle of position x & y and scale
  }
  
  void randomizeBlackHole()
  {
    position.x = random(-Window.windowWidth, Window.windowWidth);
    position.y = random(-Window.windowHeight, Window.windowHeight);
  }
}
