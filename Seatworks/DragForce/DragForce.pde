Mover mover = new Mover(0, 200);
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  mover.setColor(191, 131, 63, 255);
  mover.mass = 5;
}


void draw()
{
   background(255);
   
   ocean.render();
   noStroke();
   
   mover.applyGravity();
   mover.applyFriction();
   
   mover.render();
   mover.update();
   
   if (mover.position.x > Window.right)
    {
      mover.velocity.x *= -1;
      mover.position.x = Window.right;
    }
    
    // when the mover hits the ocean floor
   if(mover.position.y < Window.bottom)
   {
     mover.velocity.mult(-1);
     mover.position.y = Window.bottom; 
   }
   
   if(ocean.isCollidingWith(mover))
   {
      // apply drag force on the mover
      mover.applyForce(ocean.calculateDragForce(mover));
   }
}
