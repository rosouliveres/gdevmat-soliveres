public class Mover
{
   public PVector position = new PVector();
   public PVector velocity = new PVector();
   public PVector acceleration = new PVector();
   
   public float mass = 1;
   public float scale = 50;
   public float r = 255, g = 255, b = 255, a = 255;
   
   Mover()
   {
      position = new PVector(); 
   }
   
   Mover(float x, float y)
   {
      position = new PVector(x, y);
   }
   
   
   Mover(float x, float y, float scale)
   {
      position = new PVector(x, y);
      this.scale = scale;
   }
   
   Mover(PVector position)
   {
      this.position = position; 
   }
   
   Mover(PVector position, float scale)
   {
      this.position = position; 
      this.scale = scale;
   }
   
   public void randomWalk()
   {
      float decision = random(0, 4);
      
      if (decision == 0)
      {
         position.x ++; 
      }
      else if (decision == 1)
      {
         position.x --; 
      }
      else if (decision == 2)
      {
         position.y ++; 
      }
      else if (decision == 3)
      {
         position.y --; 
      }
   }
   
   public void setColor(float r, float g, float b, float a)
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
   }
   
   public void render()
   {
      fill(r,g,b,a);
      noStroke();
      // Updates necessary variables in every frame:
      update();
      
      // Draw the circle every frame:
      circle(position.x, position.y, scale);
   }
   
   private void update() // code for moving the object
   {
      this.velocity.add(this.acceleration);
      
      // limit mover's velocity
      this.velocity.limit(30);
      
      // add the velocity to the position EBRY PRAME
      this.position.add(this.velocity);
      
      // reset accel to 0 every frame to avoid over value:
      this.acceleration.mult(0);
   }
   
   public void applyForce(PVector force)
   {
     PVector f = PVector.div(force, this.mass);    // a = F/m
     this.acceleration.add(f);
   }
   
   public void applyGravity()
   {
     // cancel out the mass:
     PVector gravity = new PVector(0, -0.5f * this.mass, 0);
     this.applyForce(gravity);
   }
   
   public void applyFriction()
   {
     float c = 0.1;    // friction coefficient
     float normal = 1;               // normal force
     float frictionMagnitude = c * normal;
     
     PVector friction = this.velocity.get();
     friction.mult(-1);             // Newton's 3rd law
     friction.normalize();          // normalize the velocity
     friction.mult(frictionMagnitude);
     
     this.applyForce(friction);
   }
}
