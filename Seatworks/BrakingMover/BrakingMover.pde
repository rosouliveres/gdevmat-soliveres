Mover mover;
Mover[] movers;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  movers = new Mover[5];
  
  for(int i = 0; i < movers.length; i++)
  {
    mover = new Mover();
    mover.mass = random(1, 11); 
    mover.scale = mover.mass * 10;
    
    mover.position.x = Window.left;
    mover.position.y = (-80 * i) + 100;
    
    movers[i] = mover;
  }
}

PVector wind = new PVector(0.1, 0);

void draw()
{
  background(255);
  
  for(int i = 0; i < movers.length; i++)
  { 
    movers[i].applyForce(wind);
    movers[i].render();
    movers[i].update();
    
    if(movers[i].position.x > 0)
    {
      movers[i].applyFriction(0.3);
    }
    
    if(movers[i].position.x > Window.right - 280)
    {
      movers[i].velocity = new PVector(0,0);
      movers[i].position.x = Window.left + 50;
    }
  }
  
}
