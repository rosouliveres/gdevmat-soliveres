// called for initialization of first frame
void setup()
{
  size(1920, 1080, P3D);    // P3D makes the environment in the window 3D
  
  // size(1080, 720, P3D); for laptops
  
  // Moves the camera:
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180),    // camera position
          0, 0, 0,      // eye position
          0, -1, 0);    // up vector
}

//int y;

// Gets called every frame:
void draw()
{
  background(130);
  //circle(0, y, 100);      // circle(x, y, diameter)
  //y++;
  
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  //radius++;
  drawSineFunction();
  c++;
}

void drawCartesianPlane()
{
  line(300, 0, -300, 0);                  // line(x1, y1, x2, y2)
  line(0, 300, 0, -300);
  
  for (int i = -300; i <= 300; i+= 10)
  {
    line(i, -5, i, 5);
    line(-5, i, 5, i);
  }
}

void drawLinearFunction()
{
  /*
      f(x) = x + 2
      f(4) = 6 (4, 6)
      f(-5) = -3 (-5, -3)
  */
  
  for (int x = -200; x <= 200; x++)
  {
    circle(x, x+2, 1);
  }
}

void drawQuadraticFunction()
{
  /*
      f(x) = 10x^2 + 2x - 5
  */
  
  for (float x = -300; x <= 300; x+= 0.1)
  {
    circle(x * 10, (float)Math.pow(x, 2) + (2 * x) - 5, 1);
  }
}

float radius = 50;
void drawCircle()
{
  for(int x = 0; x < 360; x++)
  {
    circle((float)Math.cos(x) * radius, (float)Math.sin(x) * radius, 1);
  }
}

float a = 40;  
float b = 1;
float c = 2;
void drawSineFunction()
{
 /*
    f(x) = 4(sin(2x + 2))
 */
 
 for (float x = -200; x <= 200; x+=0.1)
 {
   color(255, 255, 255);
   circle(x * 10, a * sin((b * x) + c), 1);
 }
}
