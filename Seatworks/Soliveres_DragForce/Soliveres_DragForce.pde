Mover[] movers = new Mover[10];
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);
PVector wind = new PVector (0.5f, 0);

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  for(int i = 0; i < movers.length; i++)
  {
    movers[i] = new Mover();
    movers[i].mass = i + 3;
    movers[i].scale = 30 + (i*10);
    
    movers[i].position.x = Window.right - (300 + (70*i));
    movers[i].position.y = Window.top;
  }
}


void draw()
{
   background(0);
   
   ocean.render();
   noStroke();
   
   for(Mover m: movers)
   {
     m.applyGravity();
     m.applyFriction();

     m.render();
     m.update();
   
   if (ocean.isCollidingWith(m))
    {
      m.applyForce(ocean.calculateDragForce(m)); 
    }
    else
    {
       m.applyForce(wind);
    }
   
   if (m.position.x > Window.right)
    {
      m.velocity.x *= -1;
      m.position.x = Window.right;
    }
    
    // when the mover hits the ocean floor
   if (m.position.y < Window.bottom)
    {
      m.velocity.y *= -1; 
      m.position.y = Window.bottom;
    }
     //<>//
   }
   
}
