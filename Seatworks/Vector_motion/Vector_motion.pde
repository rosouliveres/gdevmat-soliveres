Mover mover;

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.acceleration = new PVector(0.1, 0);
}

void draw()
{
  background(255);
  
  fill(200, 0, 0);
  mover.render();
  println(mover.acceleration);
  println(mover.velocity);

  if(mover.position.x > Window.right)
  {
    // makes the mover go back to its starting pos
    mover.position.x = Window.left + 50; 
    mover.velocity = new PVector(0,0);
  }
  
}
