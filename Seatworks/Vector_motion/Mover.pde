public class Mover
{
   public PVector position = new PVector();
   public PVector velocity = new PVector();
   public PVector acceleration = new PVector();
   
   public float scale = 50;
   
   public void render()
   {
      // Updates necessary variables in every frame:
      update();
      
      // Draw the circle every frame:
      circle(position.x, position.y, scale);
   }
   
   private void update()
   {
      if(this.position.x > 0)
      {
        
        if(this.velocity.x < 0)
        {
           this.velocity.x = 2; 
        }
        
        this.velocity.sub(this.acceleration);
        
      }
      else
      {
        // add the acceleration to the velocity every frame
        this.velocity.add(this.acceleration);
      }
     
      
      // add the velocity to the position every frame
      this.position.add(this.velocity);
   }
}
