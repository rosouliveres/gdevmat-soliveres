Mover mover;
Mover[] movers;

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  movers = new Mover[10];
  
  // Make 10 movers
  for(int i = 0; i < movers.length; i++)
  {
    mover = new Mover();
    mover.mass = i + 1; 
    mover.scale = mover.mass * 10;
    
    mover.position.x = Window.left + 50;
    movers[i] = mover;
  }
}

PVector wind = new PVector(0.1f, 0);
PVector gravity = new PVector(0,-0.5f);

void draw()
{
  background(0);
  
  for(int i = 0; i < movers.length; i++)
  {
    movers[i].render();
    movers[i].update();
    
    // Below temporarily applies gravity by 
    //gravity.set(0, -0.5*movers[i].mass); 
    
    movers[i].applyForce(wind);
    movers[i].applyGravity();
  
    if(movers[i].position.y < Window.bottom)
    {
       movers[i].velocity.y *= -1;
       movers[i].position.y = Window.bottom;
    }
    
    if(movers[i].position.x > Window.right)
    {
       movers[i].velocity.x *= -1;
       movers[i].position.x = Window.right;
    }
  }
  
}
