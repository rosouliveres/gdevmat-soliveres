Mover mover;

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  
  // If we want to STOP or TELEPORT an object, change VELOCITY
  
  // modify this to make object move
  // Decceleration = PVector(-0.1, 0);
  // Update acceleration to move object
  mover.acceleration = new PVector(0.1, 0);     // affects velocity -> affects position
}


//PVector wind = new PVector(0.1f, 0);      // wind force
//PVector gravity = new PVector(0,-1);      // grav force

void draw()
{
  background(255);
  
  mover.render();
  mover.setColor(100,255,50,255);
  
  if(mover.position.x > 0)
  {
    mover.acceleration = new PVector(-0.1,0);
  }
  
  //mover.applyForce(wind);
  //mover.applyForce(gravity);
  
  if(mover.position.x > Window.right - 50)
  {
    mover.position.x = Window.left + 50; 
    mover.velocity = new PVector(0,0);
    mover.acceleration = new PVector(0.1, 0);
  }
}
