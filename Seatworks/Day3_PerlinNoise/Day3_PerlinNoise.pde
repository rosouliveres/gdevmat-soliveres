// called for initialization of first frame
void setup()
{
  size(1920, 1080, P3D);    // P3D makes the environment in the window 3D
  
  // size(1080, 720, P3D); for laptops
  
  // Moves the camera:
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.0f),    // camera position
          0, 0, 0,      // eye position
          0, -1, 0);    // up vector
  background(0);
}

float t = 0;

PerlinWalker pWalker = new PerlinWalker();
void draw()
{
  /*
  //float x = noise(Window.left, Window.right);
  //circle(x, 0 ,30);
  
  float n = noise(t);  // generates noise over time
  
  float x = map(n, 0, 1, 0, Window.top); //map(val to change, current min, current max, new min, new max)
  
  rect(Window.left + (t * 100), Window.bottom, 1, x);
  println(x);
  
  t += 0.01;      // rate of change of time
  */
  
  pWalker.render();
  
  pWalker.perlinWalk();
}
