class PerlinWalker
{
 public float xPos;
 public float yPos;
 public float diameter = 30;
 
 public float r_t = 100;
 public float g_t = 150;
 public float b_t = 200;
 
 public float x_t = 0;
 public float y_t = 0;
 public float d_t = 30;
 
 void render()
 {
  float d_n = noise(d_t);
  
  diameter = map(d_n, 0, 1, 30, 60);
  
  d_t += 0.01;
  
  noStroke();
  circle(xPos, yPos, diameter); 
 }
 
 void perlinWalk()
 {
  //fill(map(noise(r_t)), map(noise(g_t)), map(noise(b_t)), 255);
  
  float r_n = map(noise(r_t), 0, 1, 1, 255);
  float g_n = map(noise(g_t), 0, 1, 1, 255);
  float b_n = map(noise(b_t), 0, 1, 1, 255);
  
  fill(color(r_n, g_n, b_n));
  
  int decision = floor(random(8));
  
  float x_n = noise(x_t);
  float y_n = noise(y_t);
  
  switch(decision)
  {
    case 0:
      yPos += map(y_n, 0, 1, 0, 10);
      break;
    case 1:
      yPos -= map(y_n, 0, 1, 0, 10);
      break;
    case 2:
      xPos -= map(x_n, 0, 1, 0, 10);
      break;
    case 3:
      xPos += map(x_n, 0, 1, 0, 10);
      break;
    case 4:
      yPos += map(y_n, 0, 1, 0, 10);
      xPos += map(x_n, 0, 1, 0, 10);
      break;
    case 5:
      yPos += map(y_n, 0, 1, 0, 10);
      xPos -= map(x_n, 0, 1, 0, 10);
      break;
    case 6:
      yPos -= map(y_n, 0, 1, 0, 10);
      xPos += map(x_n, 0, 1, 0, 10);
      break;
    case 7:
      yPos -= map(y_n, 0, 1, 0, 10);
      xPos -= map(x_n, 0, 1, 0, 10);
      break;
   default:
     break;
  }
  
  
  r_t += 0.01;
  g_t += 0.01;
  b_t += 0.01;
  
  
  x_t += 0.1;
  y_t += 0.1;
 }
}
