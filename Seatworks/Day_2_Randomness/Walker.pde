class Walker 
{
 public float xPosition;
 public float yPosition;
 public color c;
 
 void render()
 {
   circle(xPosition, yPosition, 30); 
 }
 
 void randomWalk()
 {
   int decision = floor(random(8));
   int r = floor(random(256));
   int g = floor(random(256));
   int b = floor(random(256));
   
   c = color(r, g, b);
   fill(c);
   
   switch(decision)
   {
    case 0:
      yPosition+=10;      // up
      break;
    case 1:
      yPosition-=10;      // down
      break;
    case 2:
      xPosition+=10;      //right
      break;
    case 3:
      xPosition-=10;      // left
      break;
    case 4:             // Up right
      yPosition+=10;
      xPosition+=10;
      break;
    case 5:             // Down right
      yPosition-=10;
      xPosition+=10;
      break;
    case 6:              // Up left
      yPosition+=10;
      xPosition-=10;
      break;
    case 7:              // Down left
      yPosition-=10;
      xPosition-=10;
      break;
    default:
      break;
   }
 }
 
}
